package hr.ferit.bruno.example_79;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {
    //Use annotations for Views, no longer findViewById
    @BindView(R.id.bDisplayText) Button bDisplayText;
    @BindView(R.id.etTextEntry) EditText etTextEntry;
    @BindView(R.id.tvDisplay) TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bDisplayText)
    public void displayText(){
        String message = this.etTextEntry.getText().toString();
        tvDisplay.setText(message);
    }
}
